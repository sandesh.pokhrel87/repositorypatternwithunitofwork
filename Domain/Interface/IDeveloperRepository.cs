﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interface
{
    public interface IDeveloperRepository:IGenericRepository<Developer>
    {
        IEnumerable<Developer> GetPopularDeveloper(int count);
    }
}
