﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceLayer;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeveloperController : ControllerBase
    {
        private readonly IDeveloperProjectService _developerProjectService;

        public DeveloperController(IDeveloperProjectService developerProjectService)    
        {
            _developerProjectService = developerProjectService;
        }
        //get all the popular developers
        [HttpGet]
        public IActionResult GetPopularDevelopers([FromQuery] int count)
        {
            var popularDevelopers = _developerProjectService.GetPopularDeveloper(count);
            return Ok(popularDevelopers);
        }
        [HttpPost]
        public IActionResult AddDeveloperAndProject()
        {
            var developer = new Developer
            {
                Followers = 35,
                Name = "mukesh murgan"
            };
            var project = new Project
            {
                Name = "codewithmukesh"
            };
            _developerProjectService.Add(developer);
            _developerProjectService.Add(project);
            _developerProjectService.Save();
            return Ok();
        }
    }
}
