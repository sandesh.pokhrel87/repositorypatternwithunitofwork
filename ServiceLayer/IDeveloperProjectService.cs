﻿using Domain.Entities;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLayer
{
    public interface IDeveloperProjectService
    {
        IEnumerable<Developer> GetPopularDeveloper(int count);
        void Add(Developer developer);
        void Add(Project project);
        void Save();
    }
}
