﻿using Domain.Entities;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLayer
{
    public class DeveloperProjectService : IDeveloperProjectService
    {
        private readonly IDeveloperRepository _developerRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeveloperProjectService(IDeveloperRepository developerRepository,IProjectRepository projectRepository, IUnitOfWork unitOfWork)
        {
            _developerRepository = developerRepository;
            _projectRepository = projectRepository;
            _unitOfWork = unitOfWork;            
        }
        public IEnumerable<Developer> GetPopularDeveloper(int count)
        {
            return _developerRepository.GetPopularDeveloper(count);
        }
        public void Add(Developer developer)
        {
            _developerRepository.Add(developer);
        }

        public void Add(Project project)
        {
            _projectRepository.Add(project);
        }
        public void Save()
        {
            _unitOfWork.Complete();
        }
        
    }
}
